<?php
namespace TExAPITestTest\Colletion;

use TExAPITest\Entity\AutomovelEntityAbstract;
use TExAPITest\Entity\CarroEntity;
use PHPUnit\Framework\TestCase;
use Zend\Expressive\Router\RouterInterface;

class CarroEntityTest extends TestCase
{
    protected function setUp()
    {
    }

    public function testReturnCarroEntityIfImplementAutomovelEntityAbstract()
    {
        $carroEntity = new CarroEntity();
        $this->assertInstanceOf(AutomovelEntityAbstract::class, $carroEntity);
    }

}
