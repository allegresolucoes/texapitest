<?php
namespace TExAPITestTest\Colletion;

use PHPUnit\Framework\TestCase;
use TExAPITest\Collection\CarroCollection;
use TExAPITest\Entity\CarroEntity;

class CarroCollectionTest extends TestCase
{
    /** @var RouterInterface */
    protected $router;

    protected function setUp() {}

    public function testAdd()
    {
        $carroCollection = new CarroCollection();
        $carroEntity = new CarroEntity();
        $response = $carroCollection->add(stdClass);
        $this->assertInstanceOf(CarroCollection::class, $response);
        return $response;
    }
    
    public function testRemove()
    {
    	$carroCollection = new CarroCollection();
    	$carroEntity = new CarroEntity();
    	$response = $carroCollection->remove(stdClass);
    	$this->assertNull($response);
    }
}