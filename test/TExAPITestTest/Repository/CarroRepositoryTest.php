<?php
namespace TExAPITestTest\Repository;

use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Zend\Expressive\Router\RouterInterface;
use Interop\Container\ContainerInterface;

class CarroRepositoryTest extends TestCase
{
    /** @var RouterInterface */
    protected $router;
    protected $container;

    protected function setUp()
    {
        $this->router = $this->prophesize(RouterInterface::class);
        $this->container = $this->prophesize(ContainerInterface::class);
        $router = $this->prophesize(RouterInterface::class);

        $this->container->get(RouterInterface::class)->willReturn($router);
    }

    public function testReturnApagar()
    {
    }
}