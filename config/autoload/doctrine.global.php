<?php

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'params' => [
                    'host' => 'localhost',
                    'port' => '3306',
                    'user' => 'root',
                    'password' => '',
                    'dbname' => 'texapitest',
                    'driverOptions' => [
                        \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
                    ]
                ]
            ]
        ],
        'driver' => [
            'orm_default' => [
                'drivers' => [
                    'TExAPITest\Entity' => 'TExAPITest_driver'
                ]
            ],
        	'TExAPITest_driver' => [
        		'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
        		'cache' => 'array',
        		'paths' => [__DIR__ . '/../../src/TExAPITest/src/Entity']
        	]
        ]
    ]
];