<?php

/**
 * Carros
 */
$app->get('/api/v1/carros', TExAPITest\Action\BuscarCarros::class, 'carros.buscar');
$app->get('/api/v1/carros/{id}', TExAPITest\Action\BuscarCarroPorId::class, 'carros.buscarporid');
$app->post('/api/v1/carros', TExAPITest\Action\NovoCarro::class, 'carros.novo');
$app->put('/api/v1/carros/{id}', TExAPITest\Action\AlterarCarro::class, 'carros.alterar');
$app->delete('/api/v1/carros/{id}', TExAPITest\Action\ApagarCarro::class, 'carros.apagar');