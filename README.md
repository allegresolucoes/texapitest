Criar uma API REST (HAL+JSon) de um CRUD usando PHP >= 7 considerando diagrama de classes abaixo:

AutomovelEntityAbstract

	id: integer
	placa: string
	rodas: integer
	getId() : integer
	setId( $id ) : self
	getPlaca() : string
	setPlaca( $placa ) : self
	getRodas() : integer
	setRodas( $rodas ) : self

CarroEntity

	modelo: string
	getModelo() : string
	setModelo( $modelo ) : self


CarroCollection

	colecao: string
	add($entidade) : self
	remove($entidade) : self
	current() : CarroEntity
	key() : integer
	next() : void
	rewind() : void
	valid() : boolean
	count() : integer

CarroRepository

	colecao: CarroCollection
	buscarPorId($id) : CarroEntity
	buscarTodos($limite, $posicao) : CarroCollection
	buscarPor($params, $limite, $posicao) : CarroCollection
	novo($entidade) : CarroEntity
	alterar($entidade) : CarroEntity
	apagar($id) : boolean
	

Considera��es:

	Namespace da API: TExAPITest
	Este c�digo dever� estar versionado em um reposit�rio GIT.
	A placa dever� ser no formado de tr�s letras e quatro n�meros serapados por h�fen. Ex: AAA-1234
	Aplicar teste unit�rio
 
Pontos a serem avaliados:

	Organiza��o do c�digo
	Organiza��o de migrations
	Aplica��o de Design Patterns
	Conceito OOP
	Boas pr�ticas de programa��o
 
Diferenciais:

	Uso do framework Zend Expressive (2)
	Uso de ORM (Doctrine 2)
	Aplicar teste de comportamento