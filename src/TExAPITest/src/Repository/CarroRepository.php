<?php
namespace TExAPITest\Repository;

use TExAPITest\Collection\CarroCollection;
use TExAPITest\Entity\CarroEntity;
use Doctrine\ORM\EntityManager;

class CarroRepository
{
    private $entityManager;
    private $carroRepository;
    private $carroColletion;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->carroRepository = $this->entityManager
        							  ->getRepository(CarroEntity::class);
    }

    public function buscaPorId(integer $id) : CarroEntity
    {
        return $this->entityManager
        			->find(CarroEntity::class, $id);
    }

    public function buscarTodos(
    	integer $limite = null, 
    	integer $posicao =null
    ) : CarroCollection 
    {
    	$result = $this->repository
    				   ->findAll($limite, $posicao);
    	
    	$this->carroColletion = new CarroCollection();
    	
    	foreach ($result as $data) {
    		$this->carroCollection->add($data);
    	}
    		
    	return $this->carroCollection;
    }

    public function buscarPor(
    	array $params = [], 
    	integer $limite = null, 
    	integer $posicao = null
    ) : CarroColletion
    {
    	$result = $this->repository
    				   ->findBy($params, [], $limite, $posicao);
    	
    	$this->carroColletion = new CarroCollection();
    					 
    	foreach ($result as $data) {
    		$this->carroCollection->add($data);
    	}
    		
    	return $this->carroCollection;
    }

    public function novo(CarroEntity $entidade) : CarroEntity
    {
        $this->entityManager->persist($entidade);
        $this->entityManager->flush();
        return $entidade;
    }

    public function alterar(CarroEntity $entidade) : CarroEntity
    {
        $this->entityManager->persist($entidade);
        $this->entityManager->flush();
        return $entidade;
    }

    public function apagar(integer $id) : bool
    {
        $entidade = $this->entityManager->find(CarroEntity::class,$id);

        if ($entidade) {
            $this->entityManager->remove($entidade);
            $this->entityManager->flush();
            return true;
        }

        return false;
    }
}