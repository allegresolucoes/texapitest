<?php

namespace TExAPITest\Collection;

/**
 * Class CarroCollection
 * @package TExAPITest\Collection
 */
class CarroCollection
{
    /**
     * @var array
     */
    protected $colecao;
    
    /**
     * @param $entidade
     * @return $this
     */
    public function add($entidade)
    {
    	$this->colecao[(method_exists($entidade, "getId") ? $entidade->getId() : $entidade)] = $entidade;
    	return $this;
    }
    
    /**
     * @param $entidade
     */
    public function remove($entidade)
    {
    	unset($this->colecao[(method_exists($entidade, "getId") ? $entidade->getId() : $entidade)]);
    }

    /* (non-PHPdoc)
    * @see /Iterator::current()
    */
    public function current()
    {
        return current($this->colecao);
    }

    /* (non-PHPdoc)
    * @see /Iterator::key()
    */
    public function key()
    {
        return key($this->colecao);
    }

    /* (non-PHPdoc)
    * @see /Iterator::next()
    */
    public function next()
    {
        return next($this->colecao);
    }

    /* (non-PHPdoc)
    * @see /Iterator::rewind()
    */
    public function rewind()
    {
        return reset($this->colecao);
    }

    /* (non-PHPdoc)
    * @see /Iterator::valid()
    */
    public function valid()
    {
        return isset($this->colecao[$this->key()]);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->colecao);
    }
}