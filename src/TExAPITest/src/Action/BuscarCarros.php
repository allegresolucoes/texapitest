<?php
namespace TExAPITest\Action;

use TExAPITest\Repository\CarroRepository;
use Doctrine\ORM\EntityManager;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class BuscarCarros implements ServerMiddlewareInterface
{
    private $entityManager;
    private $carroRepository;

    public function __construct(
        EntityManager $entityManager,
        CarroRepository $carroRepository
    ) {
        $this->entityManager = $entityManager;
    	$this->carroRepository = $carroRepository;
    }

    public function process(
        ServerRequestInterface $request,
        DelegateInterface $delegate
    ) {
    	if ($request->getQuery()) {
    		$entities_array = $this->getArray(
    			$this->carroRepository
    				 ->buscarPor($request->getQuery())
    		);
    	} else {
	    	$entities_array = $this->getArray(
	    		$this->carroRepository
	    			 ->buscarTodos()
    		);
    	}
    	
        return new JsonResponse(
        	$entities_array
       	);
    }
    
    public function getArray(CarroCollection $carroCollection)
    {
    	$entities = [];
    	
    	foreach ($carroCollection as $entity) {
    		$entities[] = [
	    		'id' => $entity->getId(),
				'modelo' => $entity->getModelo(),
				'placa' => $entity->getPlaca(),
				'rodas' => $entity->getRodas(),
	    	];
    	}
    	
    	return $entities;
    }
}