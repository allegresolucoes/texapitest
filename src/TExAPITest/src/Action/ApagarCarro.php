<?php
namespace TExAPITest\Action;

use TExAPITest\Repository\CarroRepository;
use Doctrine\ORM\EntityManager;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class ApagarCarro implements ServerMiddlewareInterface
{
    private $entityManager;
    private $carroRepository;

    public function __construct(
        EntityManager $entityManager,
        CarroRepository $carroRepository
   	) {
        $this->entityManager = $entityManager;
        $this->carroRepository = $carroRepository;
    }

    public function process(
        ServerRequestInterface $request,
        DelegateInterface $delegate
    ) {
    	
    	try {
	    	$id = $request->getParam('id');
	        
	    	if (is_numeric($id) &&
	    		$this->carroRepository
	    			  ->apagar($id)
	    	) {
                return new JsonResponse([
                	'status' => 'success'
                 ], 200);
	    	}
    	} catch (\Exception $e) {
    		return new JsonResponse([
    			'message' => $e->getMessage()
    		], 400);
    	}
    }
}