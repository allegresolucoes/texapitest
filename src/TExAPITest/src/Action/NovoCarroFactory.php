<?php
namespace TExAPITest\Action;

use TExAPITest\Repository\CarroRepository;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use TExAPITest\Action\NovoCarro as Action;

class NovoCarroFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $em = $container->get(EntityManager::class);
        $repository = new CarroRepository($em);
        return new Action($em, $repository);
    }
}
