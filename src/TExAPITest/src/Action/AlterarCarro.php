<?php
namespace TExAPITest\Action;

use TExAPITest\Entity\CarroEntity;
use TExAPITest\Repository\CarroRepository;
use Doctrine\ORM\EntityManager;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class AlterarCarro implements ServerMiddlewareInterface
{
	private $entityManager;
	private $carroRepository;
	
	public function __construct(
		EntityManager $entityManager,
		CarroRepository $carroRepository
	) {
		$this->entityManager = $entityManager;
		$this->carroRepository = $carroRepository;
	}
	
	public function process(
		ServerRequestInterface $request,
		DelegateInterface $delegate
	) {
		try {
			$data = $request->getParsedBody();
			
			$entity = $this->entityManager
					       ->find(
				       			CarroEntity::class, 
					       		$request->getParam('id')
					  		);
			$entity->setPlaca($data['placa']);
			$entity->setRodas($data['rodas']);
			$entity->setModelo($data['modelo']);
			return new JsonResponse(
				$this->getArray(
					$this->carroRepository
						 ->alterar($entity)
				)
			);
		} catch (\Exception $e) {
			return new JsonResponse([
				'message' => $e->getMessage()
			], 400);
		}
	}
	
	public function getArray($entity)
	{
		return [
			'id' =>  $entity->getId(),
			'modelo' =>  $entity->getModelo(),
			'placa' =>  $entity->getPlaca(),
			'rodas' =>  $entity->getRodas(),
		];
	}
}