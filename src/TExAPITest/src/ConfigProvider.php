<?php

namespace TExAPITest;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies()
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'factories'  => [
            	Action\ApagarCarro::class => Action\ApagarCarroApagarCarro::class,
            	Action\AlterarCarro::class => Action\AlterarCarro::class,
            	Action\NovoCarro::class => Action\NovoCarro::class,
            	Action\BuscarCarros::class => Action\BuscarCarros::class,
            	Action\BuscarCarroPorId::class => Action\BuscarCarroPorId::class,
            ],
        ];
    }
}