<?php

namespace TExAPITest\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class CarroEntity
 *
 * @package TExAPITest\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="carro")
 * @ORM\HasLifecycleCallbacks
 */
class CarroEntity extends AutomovelEntityAbstract
{
	/**
	 * @ORM\Column(type="string", length=100)
	 */
	protected $modelo;
	
	public function setModelo(string $modelo) : self
	{
		$this->modelo = $modelo;
	}
	
	public function getModelo() : string
	{
		return $this->modelo;
	}
}