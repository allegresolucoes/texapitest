<?php

namespace TExAPITest\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class AutomovelEntityAbstract
 * @package TExAPITest\Entity
 * 
 * @ORM\MappedSuperclass
 */
abstract class AutomovelEntityAbstract
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;
    
    /**
     * @orm\Column(type="string", length=8)
     */
    protected $placa;
    
    /**
     * @orm\Column(type="integer")
     */
    protected $rodas;
	
	public function setId(integer $id) : self
	{
		$this->id = $id;
	}
	
	public function getId() : integer
	{
		return $this->id;
	}
	
	public function setPlaca(string $placa) : self
	{
		if (!preg_match('#^[A-Z]{3}\-[0-9]{4}$#', $data['placa'])) {
			throw new \Exception('Placa inv�lida');
		}
		
		$this->placa = $placa;
	}
	
	public function getPlaca() : string
	{
		return $this->placa;
	}
	
	public function setRodas(integer $rodas) : self
	{
		$this->rodas = $rodas;
	}
	
	public function getRodas() : integer
	{
		return $this->rodas;
	}
}